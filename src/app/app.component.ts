import { Component, Output } from '@angular/core';
import { Song } from './entity/song';
import { SongService } from './repository/song.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'fashionWeek';
  songs:Song[];
  selectedSong:Song;

  constructor(private repo:SongService) { }

  ngOnInit() {
    this.repo.findAll().subscribe(songs => this.songs = songs);
    this.selectedSong = this.songs[0];
  }

  increment(song) {
    song.likeCounter++;
  }

  decrement(song) {
    song.likeCounter--;
  }
}
