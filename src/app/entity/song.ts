
export interface Song {
    id: number;
    title: string; 
    singer: string;
    duration: Date;
    picturePath: string;
    isPlayed: boolean;
    likeCounter: number;
    parution: Date;
}