import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Song } from '../entity/song';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class SongService {

  constructor(private http:HttpClient) { }

  findAll(): Observable<Song[]> {
    return this.http.get<Song[]>('http://localhost:8000/api/song');
  }

}
